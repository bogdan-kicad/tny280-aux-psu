EESchema Schematic File Version 4
LIBS:TNY280_Preamp_PSU-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Switching:TNY280G U1
U 1 1 5D03C086
P 3550 4050
F 0 "U1" H 3220 4096 50  0000 R CNN
F 1 "TNY280G" H 3220 4005 50  0000 R CNN
F 2 "Package_DIP:PowerIntegrations_SMD-8C" H 3550 4050 50  0001 C CIN
F 3 "http://www.powerint.com/sites/default/files/product-docs/tny274-280.pdf" H 3550 4050 50  0001 C CNN
	1    3550 4050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 5D03C795
P 2650 1200
F 0 "J1" H 2550 1350 50  0000 C CNN
F 1 "DC IN" H 2500 1200 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2650 1200 50  0001 C CNN
F 3 "~" H 2650 1200 50  0001 C CNN
	1    2650 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D2
U 1 1 5D0401EB
P 3350 1500
F 0 "D2" V 3396 1421 50  0000 R CNN
F 1 "D_Zener" V 3305 1421 50  0000 R CNN
F 2 "Diode_THT:D_T-1_P10.16mm_Horizontal" H 3350 1500 50  0001 C CNN
F 3 "~" H 3350 1500 50  0001 C CNN
	1    3350 1500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3350 1200 3350 1350
$Comp
L Device:D D3
U 1 1 5D043002
P 3350 1800
F 0 "D3" V 3304 1879 50  0000 L CNN
F 1 "1N4007" V 3395 1879 50  0000 L CNN
F 2 "Diode_SMD:D_SMA" H 3350 1800 50  0001 C CNN
F 3 "~" H 3350 1800 50  0001 C CNN
	1    3350 1800
	0    1    1    0   
$EndComp
Wire Wire Line
	3350 3750 3350 2800
$Comp
L power:GND #PWR0103
U 1 1 5D044C7B
P 3350 4350
F 0 "#PWR0103" H 3350 4100 50  0001 C CNN
F 1 "GND" H 3355 4177 50  0000 C CNN
F 2 "" H 3350 4350 50  0001 C CNN
F 3 "" H 3350 4350 50  0001 C CNN
	1    3350 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 1200 4400 1450
Wire Wire Line
	4400 1450 4650 1450
Wire Wire Line
	4650 1650 4350 1650
Wire Wire Line
	4350 1650 4350 2800
Wire Wire Line
	4350 2800 3350 2800
Connection ~ 3350 2800
$Comp
L Device:C C3
U 1 1 5D046B2E
P 4050 4450
F 0 "C3" H 4165 4496 50  0000 L CNN
F 1 "100n" H 4165 4405 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4088 4300 50  0001 C CNN
F 3 "~" H 4050 4450 50  0001 C CNN
	1    4050 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 4300 4050 4150
Wire Wire Line
	4050 4150 3950 4150
$Comp
L power:GND #PWR0104
U 1 1 5D047346
P 4050 4600
F 0 "#PWR0104" H 4050 4350 50  0001 C CNN
F 1 "GND" H 4055 4427 50  0000 C CNN
F 2 "" H 4050 4600 50  0001 C CNN
F 3 "" H 4050 4600 50  0001 C CNN
	1    4050 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D6
U 1 1 5D055802
P 5600 2600
F 0 "D6" H 5600 2476 50  0000 C CNN
F 1 "D_Schottky" H 5600 2475 50  0001 C CNN
F 2 "Diode_THT:D_T-1_P10.16mm_Horizontal" H 5600 2600 50  0001 C CNN
F 3 "~" H 5600 2600 50  0001 C CNN
	1    5600 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:CP C6
U 1 1 5D055BEB
P 5900 2850
F 0 "C6" H 6018 2896 50  0000 L CNN
F 1 "1000u/25V" H 6018 2805 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D12.5mm_P5.00mm" H 5938 2700 50  0001 C CNN
F 3 "~" H 5900 2850 50  0001 C CNN
	1    5900 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0106
U 1 1 5D057A63
P 5150 2350
F 0 "#PWR0106" H 5150 2100 50  0001 C CNN
F 1 "GNDA" H 5155 2177 50  0000 C CNN
F 2 "" H 5150 2350 50  0001 C CNN
F 3 "" H 5150 2350 50  0001 C CNN
	1    5150 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2600 5300 2600
Wire Wire Line
	5750 2600 5900 2600
Wire Wire Line
	5900 2600 5900 2700
$Comp
L power:GNDA #PWR0107
U 1 1 5D05A941
P 5900 3000
F 0 "#PWR0107" H 5900 2750 50  0001 C CNN
F 1 "GNDA" H 5905 2827 50  0000 C CNN
F 2 "" H 5900 3000 50  0001 C CNN
F 3 "" H 5900 3000 50  0001 C CNN
	1    5900 3000
	1    0    0    -1  
$EndComp
$Comp
L Isolator:PC817 U2
U 1 1 5D05B34D
P 4850 4050
F 0 "U2" H 4850 4375 50  0000 C CNN
F 1 "PC817" H 4850 4284 50  0000 C CNN
F 2 "Package_DIP:DIP-4_W7.62mm" H 4650 3850 50  0001 L CIN
F 3 "http://www.soselectronic.cz/a_info/resource/d/pc817.pdf" H 4850 4050 50  0001 L CNN
	1    4850 4050
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5D05CD04
P 4500 4250
F 0 "#PWR0108" H 4500 4000 50  0001 C CNN
F 1 "GND" H 4505 4077 50  0000 C CNN
F 2 "" H 4500 4250 50  0001 C CNN
F 3 "" H 4500 4250 50  0001 C CNN
	1    4500 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4250 4500 4150
Wire Wire Line
	4500 4150 4550 4150
Wire Wire Line
	4550 3950 3950 3950
$Comp
L Connector:Conn_01x04_Male J2
U 1 1 5D05F27A
P 9100 2700
F 0 "J2" H 9072 2582 50  0000 R CNN
F 1 "12V" H 9072 2673 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 9100 2700 50  0001 C CNN
F 3 "~" H 9100 2700 50  0001 C CNN
	1    9100 2700
	-1   0    0    1   
$EndComp
$Comp
L power:GNDA #PWR0109
U 1 1 5D060BFF
P 8800 2850
F 0 "#PWR0109" H 8800 2600 50  0001 C CNN
F 1 "GNDA" H 8805 2677 50  0000 C CNN
F 2 "" H 8800 2850 50  0001 C CNN
F 3 "" H 8800 2850 50  0001 C CNN
	1    8800 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D7
U 1 1 5D066DB6
P 7750 3250
F 0 "D7" V 7704 3329 50  0000 L CNN
F 1 "12V" V 7795 3329 50  0000 L CNN
F 2 "Diode_THT:D_T-1_P10.16mm_Horizontal" H 7750 3250 50  0001 C CNN
F 3 "~" H 7750 3250 50  0001 C CNN
	1    7750 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	7750 3100 7750 2600
Connection ~ 7750 2600
$Comp
L Device:R R3
U 1 1 5D0690AE
P 7750 3650
F 0 "R3" H 7820 3696 50  0000 L CNN
F 1 "390" H 7820 3605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7680 3650 50  0001 C CNN
F 3 "~" H 7750 3650 50  0001 C CNN
	1    7750 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3500 7750 3400
Wire Wire Line
	7750 3800 7750 3950
$Comp
L power:GNDA #PWR0110
U 1 1 5D06BD3C
P 5350 4300
F 0 "#PWR0110" H 5350 4050 50  0001 C CNN
F 1 "GNDA" H 5355 4127 50  0000 C CNN
F 2 "" H 5350 4300 50  0001 C CNN
F 3 "" H 5350 4300 50  0001 C CNN
	1    5350 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 4300 5350 4150
Wire Wire Line
	5350 4150 5150 4150
$Comp
L Device:R R4
U 1 1 5D06DA88
P 7750 4200
F 0 "R4" H 7820 4246 50  0000 L CNN
F 1 "2k" H 7820 4155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7680 4200 50  0001 C CNN
F 3 "~" H 7750 4200 50  0001 C CNN
	1    7750 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 4050 7750 3950
Connection ~ 7750 3950
$Comp
L power:GNDA #PWR0111
U 1 1 5D06F500
P 7750 4350
F 0 "#PWR0111" H 7750 4100 50  0001 C CNN
F 1 "GNDA" H 7755 4177 50  0000 C CNN
F 2 "" H 7750 4350 50  0001 C CNN
F 3 "" H 7750 4350 50  0001 C CNN
	1    7750 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 2850 8800 2800
Wire Wire Line
	8800 2700 8900 2700
$Comp
L Device:LED D8
U 1 1 5D08D812
P 8350 2850
F 0 "D8" V 8389 2733 50  0000 R CNN
F 1 "LED" V 8298 2733 50  0000 R CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 8350 2850 50  0001 C CNN
F 3 "~" H 8350 2850 50  0001 C CNN
	1    8350 2850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 5D08E292
P 8350 3300
F 0 "R5" H 8420 3346 50  0000 L CNN
F 1 "560" H 8420 3255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8280 3300 50  0001 C CNN
F 3 "~" H 8350 3300 50  0001 C CNN
	1    8350 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0112
U 1 1 5D08E62B
P 8350 3450
F 0 "#PWR0112" H 8350 3200 50  0001 C CNN
F 1 "GNDA" H 8355 3277 50  0000 C CNN
F 2 "" H 8350 3450 50  0001 C CNN
F 3 "" H 8350 3450 50  0001 C CNN
	1    8350 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 3150 8350 3000
Wire Wire Line
	8350 2700 8350 2600
Wire Wire Line
	7750 2600 8350 2600
Wire Wire Line
	8350 2600 8800 2600
Connection ~ 8350 2600
Wire Wire Line
	3350 1200 4400 1200
Wire Wire Line
	3350 1950 3350 2800
Wire Wire Line
	5300 1450 5050 1450
Wire Wire Line
	5300 1450 5300 2600
$Comp
L pspice:INDUCTOR L1
U 1 1 5D29F9D8
P 6350 2600
F 0 "L1" H 6350 2815 50  0000 C CNN
F 1 "Ferrite_Bead" H 6350 2724 50  0000 C CNN
F 2 "Inductor_THT:L_Axial_L7.0mm_D3.3mm_P5.08mm_Vertical_Fastron_MICC" H 6350 2600 50  0001 C CNN
F 3 "~" H 6350 2600 50  0001 C CNN
	1    6350 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 2600 5900 2600
Connection ~ 5900 2600
$Comp
L Device:CP C2
U 1 1 5D2A12E0
P 6800 2850
F 0 "C2" H 6918 2896 50  0000 L CNN
F 1 "1000u/25V" H 6918 2805 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D12.5mm_P5.00mm" H 6838 2700 50  0001 C CNN
F 3 "~" H 6800 2850 50  0001 C CNN
	1    6800 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR0105
U 1 1 5D2A12E6
P 6800 3000
F 0 "#PWR0105" H 6800 2750 50  0001 C CNN
F 1 "GNDA" H 6805 2827 50  0000 C CNN
F 2 "" H 6800 3000 50  0001 C CNN
F 3 "" H 6800 3000 50  0001 C CNN
	1    6800 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 2700 6800 2600
Wire Wire Line
	6800 2600 6600 2600
Wire Wire Line
	6800 2600 7750 2600
Connection ~ 6800 2600
Wire Wire Line
	5150 3950 7750 3950
$Comp
L My_Library:Gate_Drive_Trafo TR1
U 1 1 5D2A4A4E
P 4850 1550
F 0 "TR1" H 4850 2065 50  0000 C CNN
F 1 "Gate_Drive_Trafo" H 4850 1974 50  0000 C CNN
F 2 "MyLibrary:Gate_Drive_Trafo" H 4900 1400 50  0001 C CNN
F 3 "" H 4900 1400 50  0001 C CNN
	1    4850 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1650 5050 1650
Wire Wire Line
	5150 1650 5150 2350
Wire Wire Line
	8900 2800 8800 2800
Connection ~ 8800 2800
Wire Wire Line
	8800 2800 8800 2700
Wire Wire Line
	8900 2500 8800 2500
Wire Wire Line
	8800 2500 8800 2600
Connection ~ 8800 2600
Wire Wire Line
	8800 2600 8900 2600
$Comp
L power:GND #PWR0101
U 1 1 5D4B5CD7
P 2900 1500
F 0 "#PWR0101" H 2900 1250 50  0001 C CNN
F 1 "GND" H 2905 1327 50  0000 C CNN
F 2 "" H 2900 1500 50  0001 C CNN
F 3 "" H 2900 1500 50  0001 C CNN
	1    2900 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1500 2900 1400
Wire Wire Line
	2900 1400 2850 1400
Wire Wire Line
	2850 1300 2900 1300
Wire Wire Line
	2900 1300 2900 1400
Connection ~ 2900 1400
Wire Wire Line
	2850 1200 3350 1200
Connection ~ 3350 1200
Wire Wire Line
	3350 1200 3350 1100
Wire Wire Line
	3350 1100 2850 1100
$EndSCHEMATC
